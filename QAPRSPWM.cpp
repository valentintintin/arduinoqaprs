/*
	Copyright (C) 2019 Valentin SAUGNIER F4HVV

    This file is part of ArduinoQAPRS.

    ArduinoQAPRS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ArduinoQAPRS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ArduinoQAPRS; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Ten plik jest częścią ArduinoQAPRS.

    ArduinoQAPRS jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
    i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
    wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
    Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

    Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
    użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
    gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
    ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
    Powszechnej Licencji Publicznej GNU.

    Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
    Powszechnej Licencji Publicznej GNU (GNU General Public License);
    jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
    Place, Fifth Floor, Boston, MA  02110-1301  USA

 */
#include "QAPRSPWM.h"

void QAPRSPWM::toggleTone() {
	QAPRSBase::toggleTone();
	if (this->currentTone == QAPRSMark){
		 this->timer1StartValue = this->toneMarkTime;
	} else {
		 this->timer1StartValue = this->toneSpaceTime;
	}
	TCNT1 = this->timer1StartValue;
}

/**
 * Włącz nadawania. W tej implementacji uruchom także Timer1
 */
void QAPRSPWM::enableTransmission() {
    QAPRSBase::enableTransmission();
    TCCR1B=(1<<CS10);
}

/**
 * Wyłącz nadawanie. W tej implementacji zatrzymaj także Timer1
 */
void QAPRSPWM::disableTranssmision() {
    QAPRSBase::disableTranssmision();
    TCCR1B&= ~ ((1<<CS12)|(1<<CS11)|(1<<CS10));
}

void QAPRSPWM::timerInterruptHandler() {
    static bool state;

    if (state) {
        state = false;
    } else {
        state = true;
    }
    digitalWrite(audioOutPin, state);

    TCNT1 = this->timer1StartValue;
}

uint8_t QAPRSPWM::canTransmit() {
	if (this->sensePin){
		if (this->sensePin > 0){
			return !digitalRead(this->sensePin);
		} else {
			return digitalRead(abs(this->sensePin));
		}
	} else {
		return 1;
	}
}

void QAPRSPWM::setVariant(QAPRSVariant variant) {
	QAPRSBase::setVariant(variant);
	switch(variant){
	case QAPRSVHF:
		this->toneMarkTime = this->toneMarkTimeVHF;
		this->toneSpaceTime = this->toneSpaceTimeVHF;
		break;
	case QAPRSHF:
		this->toneMarkTime = this->toneMarkTimeHF;
		this->toneSpaceTime = this->toneSpaceTimeHF;
		break;
	}
}

void QAPRSPWM::init(int8_t sensePin, int8_t txEnablePin, int8_t audioOutPin) {
	QAPRSBase::init(sensePin, txEnablePin);
	this->audioOutPin = audioOutPin;
	pinMode(audioOutPin, OUTPUT);
}

void QAPRSPWM::init(int8_t sensePin, int8_t txEnablePin, char* from_addr, uint8_t from_ssid, char* to_addr, uint8_t to_ssid, char* relays, int8_t audioOutPin) {
	QAPRSBase::init(sensePin, txEnablePin, from_addr, from_ssid, to_addr, to_ssid, relays);
	this->audioOutPin = audioOutPin;
	pinMode(audioOutPin, OUTPUT);
}
